//console.log ("Hello world");

//[Section] Function
	//Function
		//Functions in javascript are lines/blocks of codes that tell our device/application to perform a certain task when called or invoke.
		// Functions are mostly created to create a complicated tasks to run several lines of code of succession.
		// They are also used to prevent repeating lines/blocks of codes that perform the same task or function.
	//Function declaration
		//(function statements)-defines a function with specified parameters.

		/* Syntax:
			function functionName(){
				code block (statements)
			}
		*/
	// function keyword - used to define a javascript functions
	// functionName - the function name. Functions are named to be able to use later in the code.
	// function block ({}) - the statements which comprise the body of the function. This is where the code will be executed.
		function printName(){
			console.log("My name is John.");
			console.log("My last name is Dela Cruz.");
		}

	// Function invocation - the code block and statement inside a function is not immediately executed when the function is defined/declared. The code block and statements indside a function is executed when the function is invoked.
		// It is common to use the term "call a function" instead of "invoke a function"

			//"Let's invoke the function that we declared."
			printName();
			//functions are reusable.
			printName();
// [Sections] function Declaration and function expression
	//Function Declaration
		// A function can be created through function declaration by using the keyword function and adding function name.

		//Declared functions are not executed immediately.
			
			declaredFunction();

			function declaredFunction(){
				console.log("Hello World from declaredFunction");
			}

		//function expression
			// a function can also be stored in variable. This is called function expression.

			// Anonymous function - functions without a name.

			let variableFunction = function(){
				console.log("Hello from variableFunction!");
			}
			variableFunction();

		let functionExpression = function funcName (){
			console.log("Hello from functionExpression!");
		}

		functionExpression();

	// You can reassign declared functions and function expression to new anonymous function.

		declaredFunction = function(){
			console.log("updated declaredFunction.");
		}

		declaredFunction();

		functionExpression = function(){
			console.log("update functionExpression");
		}

		functionExpression();

		//Function expression using const keyword
		const constantFunc = function(){
			console.log ("Initialized with const!");
		}
		constantFunc();

		/*constantFunc = function (){
			console.log("Cannot be reassigned!");
		}

		constantFuncn();*/

	// [Section] Function Scoping

/*
		Scope is accessibility of variable within our program.

		Javascript Variables, it has 3 types of scope:
		1. local/block scope
		2. global scope
		3. function scope
*/

		{
			let localVar = "Armando Perez";
			console.log(localVar);

		}

	let globalVar = "Mr. Worldwide";
	// console.log(localVar);
	console.log(globalVar);

	//Function scope
	// Javascript has function scope: Each function creates a new scope.
	// Variables defined inside a function are not accesible outside the function.

	function showName(){
		let functionLet = "Jane123";
		const functionConst = "John123";

		console.log(functionLet, functionConst);

	}

	showName();
		//(error) The variables, functionLet, functionConst, are function scoped and cannot be accessed outside of the function that they were declared. 
	//console.log(functionLet, functionConst);

// [Section] Nested Functions
	//You can created another function inside a function. This is called nested function. This is called a nested function.

	function myNewFunction(){
		let name = "Jane";
		console.log(name);
		function nestedFunction() {
			let nestedName = "John"
			console.log(nestedName);
			console.log(name);
		}
		nestedFunction();
	} 
	myNewFunction();

	//Function and Global Scoped Variables
		// Global Scoped Variable
		let globalName = "Alexandro";

		function myNewFunction2() {
			let nameInside = "Renz";

			console.log(globalName);
			console.log(nameInside);
		}

		myNewFunction2()

// [Section] Using Alert
		// alert() allows us to show a small window at the top of our browser page to show information to our users. As opposed to console.log() which only shows on the console. It allows us to show a short dialog or instruction to our users. The page will wait until the user dismiss the dialog.

		alert ("Hello World"); //This will run immediately when page loads.

		//syntax:
		//alert("<messageInString>");

		function showSampleAlert(){
			alert("Hello, User!");
		}

		showSampleAlert();

		// Notes on use of alert ()
			// Show only an alert()for short dialogs/messages to the user
			//	Do not overuse alert() because the program/js has to wait for it to be dismissed before continuing.

// [Section] prompt()
		//prompt allows us to show a small window at the top of the browser to gather user input from the prompt () will be returbed as a string once the user dismisses the window.
	let samplePrompt = prompt("Enter your name: "); 
	console.log(samplePrompt);
	console.log(typeof samplePrompt);

	/*
	Syntax:
		prompt("<dialogInString>")
	*/

	let sampleNullPrompt = prompt ("Do not enter anything ")
	console.log(sampleNullPrompt);
	console.log(typeof sampleNullPrompt);

	function printWelcomeMessages(){
		let firstName = prompt("Enter your First Name: ");
		let lastName = prompt("Enter your Last Name: ");
	
		console.log ("Hello, " + firstName +" "+ lastName+"!");
	}
	printWelcomeMessages();

// [Section] Function Naming Convention
		// Function names should be definitive of the task it will perform. It usually contains a verb.

	function getCourses() {
		let course = ["Science 101", "Math 101", "English 101"];
		console.log(course);
	}
	getCourses();

	// Avoid generic names to avoid confusion within your code/program 
		function getName(){
			let name = "Jamie"
			console.log(name);
		}
		getName();
	// Avoid pointless and inappropriate function names.
	// name your function in small caps. Follow camelCassing when naming variables and functions.

		function displayCarInfo (){
			console.log("Brand: Toyota");
			console.log("Type: Sedan");
			console.log("Price: 1,500,000");
		}

		displayCarInfo();